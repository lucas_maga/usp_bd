package entity;

public class QueryBuilder {
	
	protected Query query = new Query();
	
	public QueryBuilder select(String... params){
		
		for (String param : params) {
			query.append(param + ", ");
		}
		return this;
	}
	
	public QueryBuilder insert(String tableName){
		query.append(tableName);
		return this;
	}
	
	public QueryBuilder update(String tableName){
		query.append(tableName);
		return this;
	}
	
	public QueryBuilder set(String colum, String param){
		query.append("SET " + colum + " = '" + param + "'");
		return this;
	}
	
	public QueryBuilder andSet(String colum, String param){
		query.append(", " + colum + " = '" + param + "'");
		return this;
	}
	
	public QueryBuilder values(String... params){
		
		query.append(" VALUES (");
		for (String param : params) {
			query.append("'" + param + "'" + ", ");
		}
		
		query.append(")");
		return this;
		
	}
	
	public QueryBuilder delete(){ 
		return this;	
	}
	
	public QueryBuilder from (String from){
		query.append("FROM " + from);
		return this;
	}
	
	public QueryBuilder where (String colum, String param){
		query.append("WHERE " + colum + " = '" + param + "'");
		return this;
	}
	
	public QueryBuilder and (String colum, String param){
		query.append("AND " + colum + " = '" + param + "'");
		return this;
	}
	
	/**
	 * Builders
	 */
	
	public String buildSelect(){ return query.getSelect(); }
	
	public String buildDelete(){ return query.getDelete(); }
	
	public String buildInsert(){ return query.getInsert(); }
	
	public String buildUpdate(){ return query.getUpdate(); }

}
