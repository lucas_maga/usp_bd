package entity;

public class Query {
	
	private StringBuilder update = new StringBuilder();
	private StringBuilder select = new StringBuilder();
	private StringBuilder delete = new StringBuilder();
	private StringBuilder insert = new StringBuilder();
	
	private StringBuilder query = new StringBuilder();
	
	public Query(){
		select.append("SELECT ");
		update.append("UPDATE ");
		delete.append("DELETE ");
		insert.append("INSERT INTO ");
	}
	
	public Query append(String appender){
		query.append(appender + " ");
		return this;
	}
	
	public String getSelect(){	return select.toString() + query;	}

	public String getDelete() {	return delete.toString() + query;	}

	public String getInsert() {	return insert.toString() + query;	}
	
	public String getUpdate() {	return update.toString() + query;	}

}
