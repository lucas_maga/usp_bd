package service;

import base.InvalidParamException;
import repository.DatabaseUtils;

public interface BaseService<T> {
	
	default T save(T t) throws InvalidParamException{
		
		if(t == null){
			throw new InvalidParamException("Parametro não pode ser nulo");
		}
		
		DatabaseUtils<T> database = new DatabaseUtils<>();
		return database.insert(t);
	}
	
	default boolean delete(T t) throws InvalidParamException{
		
		if(t == null){
			throw new InvalidParamException("Parametro não pode ser nulo");
		}
		
		DatabaseUtils<T> database = new DatabaseUtils<>();
		return database.delete();
	}
	
	default T find(Long id) throws InvalidParamException{
		
		if(id == null){
			throw new InvalidParamException("Parametro não pode ser nulo");
		}
		
		DatabaseUtils<T> database = new DatabaseUtils<>();
		return database.find(id);
	}
	

}
