package base;

/**
 * @author s2it_lmagalhaes
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 3/2/17 3:11 PM
 */
public class Equipe extends Participante {

    private Long id;

    private String nome;

    public Equipe() {
    }

    public Equipe(final Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }
}
