package base;

/**
 * @author s2it_lmagalhaes
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 3/2/17 3:11 PM
 */
public class Atleta extends Participante {

    private Long id;

    private int altura;

    private int peso;

    private Participante participante;

    private Equipe equipe;

    public Atleta() {
    }

    public Atleta(final Long id, final int altura, final int peso, final Participante participante,
            final Equipe equipe) {
        this.id = id;
        this.altura = altura;
        this.peso = peso;
        this.participante = participante;
        this.equipe = equipe;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(final int altura) {
        this.altura = altura;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(final int peso) {
        this.peso = peso;
    }

    public Participante getParticipante() {
        return participante;
    }

    public void setParticipanteId(final Participante participante) {
        this.participante = participante;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(final Equipe equipe) {
        this.equipe = equipe;
    }
}
