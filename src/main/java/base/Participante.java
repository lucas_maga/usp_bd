package base;

import java.util.Date;

/**
 * @author s2it_lmagalhaes
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 3/2/17 3:15 PM
 */
public class Participante {

    private Long id;

    private String nome;

    private String passaporte;

    private int sexo;

    private Date nascimento;

    public Participante() {
    }

    public Participante(final Long id, final String nome, final String passaporte, final int sexo,
            final Date nascimento) {
        this.id = id;
        this.nome = nome;
        this.passaporte = passaporte;
        this.sexo = sexo;
        this.nascimento = nascimento;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public String getPassaporte() {
        return passaporte;
    }

    public void setPassaporte(final String passaporte) {
        this.passaporte = passaporte;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(final int sexo) {
        this.sexo = sexo;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(final Date nascimento) {
        this.nascimento = nascimento;
    }
}
