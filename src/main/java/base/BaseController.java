package base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(BaseController.DEFAULT_URL)
@Controller
public class BaseController {

    protected final static String DEFAULT_URL = "/usp";

    @RequestMapping("/")
    public String home(Model model) {
        List<Atleta> list = new ArrayList<>();
        Participante participante = new Participante();
        Equipe equipe = new Equipe();
        Participante participante2 = new Participante();
        Equipe equipe2 = new Equipe();

        Atleta atleta = new Atleta(1L, 165, 65, participante, equipe);
        atleta.setNascimento(new Date());
        atleta.setSexo(1);
        atleta.setNome("Lucas");
        atleta.setPassaporte("123ABC");

        Atleta atleta2 = new Atleta(1L, 165, 65, participante2, equipe2);
        atleta2.setNascimento(new Date());
        atleta2.setSexo(1);
        atleta2.setNome("Sabrina");
        atleta2.setPassaporte("123ABCee");

        list.add(atleta);
        list.add(atleta2);
        model.addAttribute("users", list);
        return "index";
    }

    @RequestMapping("/view")
    public String view(@RequestParam(name = "id", required = true) final Long id, Model model) {
        System.out.println(id);
        Participante participante = new Participante();
        Equipe equipe = new Equipe();
        Atleta atleta = new Atleta(1L, 165, 65, participante, equipe);

        atleta.setNascimento(new Date());
        atleta.setSexo(1);
        atleta.setNome("Lucas");
        atleta.setPassaporte("123ABC");

        model.addAttribute("user", atleta);
        return "view";
    }

    @RequestMapping("/edit")
    public String edit(@RequestParam(name = "id", required = true) final Long id, Model model) {
        Participante participante = new Participante();
        List<Equipe> list = new ArrayList<>();

        Equipe equipe = new Equipe(1L, "equipe A");
        Equipe equipe2 = new Equipe(2L, "equipe B");

        Atleta atleta = new Atleta(1L, 165, 65, participante, equipe);
        atleta.setNascimento(new Date());
        atleta.setSexo(1);
        atleta.setNome("Lucas");
        atleta.setPassaporte("123ABC");

        list.add(equipe);
        list.add(equipe2);

        model.addAttribute("user", atleta);
        model.addAttribute("equipes", list);
        return "edit";
    }

}
